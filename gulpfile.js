'use strict';
 
var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');



//Compiler sass to css
gulp.task('sass', function() {
  return gulp.src('./sass/**/*.scss')
      .pipe(sass())
      .pipe(gulp.dest('./css'))
      .pipe(browserSync.reload({
        stream: true
      }))
});


//browser sync
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: ''
    }
  })
});


//Minify Css
gulp.task('minify-css', function() {
    return gulp.src('./css/*.css')
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/css'));
});


//Minify JS
gulp.task('minify-js', function() {
    return gulp.src('./js/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});


//Minify IMG
gulp.task('minify-img', () => {
    return gulp.src('./img/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('dist/img'));
});


//Font
gulp.task('fonts', function() {
    return gulp.src('./fonts/**/*')
        .pipe(gulp.dest('dist/fonts'))
});


//Html
gulp.task('html', function() {
    return gulp.src('index.html')
        .pipe(gulp.dest('dist/'))
});

//Watch
gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('./sass/**/*.scss', ['sass']);
  gulp.watch('./*.html', browserSync.reload);
  gulp.watch('./js/**/*.js', browserSync.reload);
});

//Build
gulp.task('build', ['minify-css', 'minify-js', 'minify-img', 'fonts', 'html']);




