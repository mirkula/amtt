// html5shiv MIT @rem remysharp.com/html5-enabling-script
// iepp v1.6.2 MIT @jon_neal iecss.com/print-protector
/*@cc_on(function(m,c){var z="abbr|article|aside|audio|canvas|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video";function n(d){for(var a=-1;++a<o;)d.createElement(i[a])}function p(d,a){for(var e=-1,b=d.length,j,q=[];++e<b;){j=d[e];if((a=j.media||a)!="screen")q.push(p(j.imports,a),j.cssText)}return q.join("")}var g=c.createElement("div");g.innerHTML="<z>i</z>";if(g.childNodes.length!==1){var i=z.split("|"),o=i.length,s=RegExp("(^|\\s)("+z+")",
"gi"),t=RegExp("<(/*)("+z+")","gi"),u=RegExp("(^|[^\\n]*?\\s)("+z+")([^\\n]*)({[\\n\\w\\W]*?})","gi"),r=c.createDocumentFragment(),k=c.documentElement;g=k.firstChild;var h=c.createElement("body"),l=c.createElement("style"),f;n(c);n(r);g.insertBefore(l,
g.firstChild);l.media="print";m.attachEvent("onbeforeprint",function(){var d=-1,a=p(c.styleSheets,"all"),e=[],b;for(f=f||c.body;(b=u.exec(a))!=null;)e.push((b[1]+b[2]+b[3]).replace(s,"$1.iepp_$2")+b[4]);for(l.styleSheet.cssText=e.join("\n");++d<o;){a=c.getElementsByTagName(i[d]);e=a.length;for(b=-1;++b<e;)if(a[b].className.indexOf("iepp_")<0)a[b].className+=" iepp_"+i[d]}r.appendChild(f);k.appendChild(h);h.className=f.className;h.innerHTML=f.innerHTML.replace(t,"<$1font")});m.attachEvent("onafterprint",
function(){h.innerHTML="";k.removeChild(h);k.appendChild(f);l.styleSheet.cssText=""})}})(this,document);@*/


$(document).ready(function() {

    //Navigation-menu
    var menuToggle = $('#js-mobile-menu').unbind();
    $('#js-navigation-menu').removeClass("show");

    menuToggle.on('click', function(e) {
        e.preventDefault();
        $('#js-navigation-menu').slideToggle(function(){
            if($('#js-navigation-menu').is(':hidden')) {
                $('#js-navigation-menu').removeAttr('style');
            }
        });
    });

    //Pop-up company

        function make_something(){
            // var list = document.getElementById('listName');
            var items = document.getElementsByClassName('item-company');
            var companys = document.body.getElementsByClassName("item-company");
            var count = 0;
            var interval = setInterval(function(){

                    //for (var i = 0; i < companys.length-1; i++) {
                    if(count<=items.length-1){
                    var company = companys[count];

                    $(company).toggleClass("item-transition");
                        count++;
                    //}
                }else{
                    clearInterval(interval);
                }
            },50)
        }
        //


    $(".dropdown-button1").click(function() {
        $(".pop-up").toggleClass("show-pop-up");
        $(".drop-icon").toggleClass("rotate");
        //$(".item-company").toggleClass("item-transition");
        make_something();

        $(".pop-up").click(function(){
            $(".pop-up").removeClass("show-pop-up");
            $(".drop-icon").removeClass("rotate");
            $(".item-company").removeClass("item-transition");
        });
    });


    //Phone spoiler
    $(".dropdown-button2").click(function() {
        $(".dropdown-menu2").toggleClass("show-menu");

        $(".dropdown-menu > li").click(function(){
            $(".dropdown-menu2").removeClass("show-menu");
        });

    });

    //Search spoiler
        $(".search-btn").click(function() {
            $(".search-bar").toggleClass("active");
        });

    //Tools spoiler
        $(".tools-btn").click(function() {
            $(".tools-btn").toggleClass("active");
            $(".tool-bar").toggleClass("translate");
        });
        $(".tools").click(function() {
            $(".tools").toggleClass("active");
            $(".tool-bar").toggleClass("translate");
        });

    //Menu btn
        $(".navigation-menu-button").click(function() {
            $(".navigation-menu-button").toggleClass("active");
        });

    //Submenu mobile
        $(".more").click(function() {
            $(".more > .submenu").toggleClass("transition");
        });

    //OWL-carousel
    var current = 0;
    var carousel = $(".owl-carousel").owlCarousel({
        items:1,
        itemsDesktop : [ , 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
        itemsMobile : [479, 1],
        navigation : false,
        pagination : false,
        lazyLoad : true,
        autoPlay : true,
        afterMove:afterMove
    });

        $('.slide_prev').click(function(){
            carousel.trigger('owl.prev')
        });
        $('.slide_next').click(function(){
            carousel.trigger('owl.next')
        });
    function afterMove(e){
        var data = carousel.data('owlCarousel');
        current = data.currentItem+1;
        $('.counted').html(current+'/'+data.itemsAmount);
    }

    //Scroll to #main
        $(".scroll-btn").click(function() {
            var scrollTop = $('#main').offset().top;
            $('body,html').animate({scrollTop: scrollTop}, 500);

        });

    //Scroll Tool-bar
        $(function(){

            var startTop = $('#owl-example').offset().top;
            var toolBar = document.getElementsByClassName('tool-bar')[0];

            toolBar.style.top = startTop + 'px';

            $(window).scroll(function() {

                var top = $(document).scrollTop();
                var startScroll = $('#owl-example').offset().top;

                if (top > startScroll) {
                    toolBar.style.top = 0 + 'px';
                } else {
                    toolBar.style.top = startTop + 'px';
                }
            });

        });


    }
);



